'use strict';

/* jshint -W098 */
// The Package is past automatically as first parameter
module.exports = function(Furnitures, app, auth, database) {

    var furnitures = require('../controllers/furnitures')(Furnitures);

    app.route('/api/furnitures')
        .get(furnitures.all)
        .post(furnitures.create);

    app.route('/api/furnitures/:furnitureId')
        .get(furnitures.show)
        .put(furnitures.update)
        .delete(furnitures.destroy);

    app.param('furnitureId', furnitures.furniture);

};
