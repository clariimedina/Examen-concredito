'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Furniture = mongoose.model('Furniture'),
    _ = require('lodash');

module.exports = function(Furnitures) {

    return {
        /**
         * Find funiture by id
         */
        furniture: function(req, res, next, id) {
            Furniture.load(id, function(err, furniture) {
                if (err) return next(err);
                if (!furniture) return next(new Error('Failed to load furniture ' + id));
                req.furniture = furniture;
                next();
            });
        },
        /**
         * Create a funiture
         */
        create: function(req, res) {
            var furniture = new Furniture(req.body);
            //furniture.user = req.user;

            furniture.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot save the furniture'
                    });
                }
                res.json(furniture);
            });
        },
        /**
         * Update an article
         */
        update: function(req, res) {
            var furniture = req.furniture;

            furniture = _.extend(furniture, req.body);


            furniture.save(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot update the furniture'
                    });
                }
                res.json(furniture);
            });
        },
        /**
         * Delete an funiture
         */
        destroy: function(req, res) {
            var furniture = req.furniture;


            furniture.remove(function(err) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot delete the furniture'
                    });
                }

                res.json(furniture);
            });
        },
        /**
         * Show an furniture
         */
        show: function(req, res) {
            res.json(req.furniture);
        },
        /**
         * List of Furnitures
         */
        all: function(req, res) {
            var query = Furniture.find({}).sort('-created').populate('user', 'name username').exec(function(err, furnitures) {
                if (err) {
                    return res.status(500).json({
                        error: 'Cannot list the furnitures'
                    });
                }

                res.json(furnitures);
            });

        }
    };
}