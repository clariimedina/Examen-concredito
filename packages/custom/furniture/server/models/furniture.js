'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


/**
 * Article Schema
 */
var FurnitureSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  articleDescription: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  type:{
    type: String,
    enum:[
      'table',
      'livingRoom',
      'kitchen',
      'office',
      'bedRoom'
    ],
    required: true
  },
  image: {
    type: String
  }
});

/**
 * Validations
 */
// FurnitureSchema.path('title').validate(function(title) {
//   return !!title;
// }, 'Title cannot be blank');

// FurnitureSchema.path('content').validate(function(content) {
//   return !!content;
// }, 'Content cannot be blank');

/**
 * Statics
 */
FurnitureSchema.statics.load = function(id, cb) {
  this.findOne({
    _id: id
  }).populate('user', 'name username').exec(cb);
};

mongoose.model('Furniture', FurnitureSchema);
