/* jshint -W098 */
angular.module('mean.furniture').controller('FurnitureController', ['$scope','$stateParams', '$location', 'Global','MeanUser', 'Furniture',
  function($scope,$stateParams, MeanUser, $location, Global, Furniture) {
    $scope.global = Global;
    $scope.package = {
      name: 'furniture'
    };

    $scope.find = function() {
      Furniture.query(function(furnitures) {
        $scope.furnitures = furnitures;
      });
    };

    
  }
]);
