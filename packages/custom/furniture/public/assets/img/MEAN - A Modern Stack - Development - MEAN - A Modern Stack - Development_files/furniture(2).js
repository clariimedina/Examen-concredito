'use strict';

angular.module('mean.furniture').factory('Furniture', [ '$resource',
  function($resource) {
    return $resource('api/furnitures/:furnitureId',{
      furnitureId: '@_id'
    },{
    	update:{
    		method:'PUT'
    	}
    });
  }
]);
