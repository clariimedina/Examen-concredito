'use strict';

angular.module('mean.furniture').config(['$stateProvider',
    function($stateProvider) {

        $stateProvider
            .state('livingRoom page', {
                url: '/furniture/livingRoom',
                templateUrl: 'furniture/views/livingRoom.html',
            })
            .state('tables page', {
                url: '/furniture/tables',
                templateUrl: 'furniture/views/tables.html'
            })
            .state('kitchen page', {
                url: '/furniture/kitchen',
                templateUrl: 'furniture/views/kitchen.html'
            })
            .state('bedRoom page', {
                url: '/furniture/beedRoom',
                templateUrl: 'furniture/views/bedRoom.html'
            })
            .state('office page', {
                url: '/furniture/office',
                templateUrl: 'furniture/views/office.html'
            })
            .state('Buy one', {
                url: '/furniture/:furnitureId/buy',
                templateUrl: 'furniture/views/buy.html',
                resolve: {
                    loggedin: function(MeanUser) {
                        return MeanUser.checkLoggedin();
                    }
                }
            })
    }
]);
