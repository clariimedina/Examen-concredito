angular.module('mean.furniture').controller('BuyController', ['$scope', '$stateParams', '$location', 'Global', 'MeanUser', 'Furniture',
    function($scope, $stateParams, location, Global, MeanUser, Furniture) {
        $scope.global = Global;
        $scope.package = {
            name: 'furniture'
        };

        console.log(MeanUser.user.email);

        $scope.findOne = function() {
            Furniture.get({
                furnitureId: $stateParams.furnitureId
            }, function(furniture) {
                $scope.furniture = furniture;
            });
        };

        $scope.getShortDescription = function(description) {
            var des = description
            console.log(description);
            return des.trunc(100, true);
        };

        String.prototype.trunc = function(n, useWordBoundary) {
            var toLong = this.length > n,
                s_ = toLong ? this.substr(0, n - 1) : this;
            s_ = useWordBoundary && toLong ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
            return toLong ? s_ + '....;' : s_;
        };

        $scope.monthlys = [{
            value: 3,
            str: '3 Abonos mensuales de: '
        }, {
            value: 6,
            str: '6 Abonos mensuales de: '
        }, {
            value: 9,
            str: '9 Abonos mensuales de: '
        }, {
            value: 12,
            str: '12 Abonos mensuales de: '
        }];

        $scope.getMonthlyDeposit = function() {

        };

        $scope.getInterests = function(months, interests, price) {
            return price * ((interests * months) / 100);
        };

        $scope.getRealPrice = function(months, price) {
            var interests = $scope.getInterests(months, 2.8, price);
            return price + interests;
        };

        $scope.getTotallyForMonth = function(months, price) {
              var totallyFormat = getTotally(months, price);

            return (totallyFormat).formatMoney(2);
        };

        var getTotally = function(months, price) {
            var realPrice = $scope.getRealPrice(months, price);
            var prePayment = getPrepayment(realPrice);
            var bonus = getBonus(realPrice);

            return(realPrice - prePayment) - bonus;
        };

        var getPrepayment = function(price) {
            return price * (20 / 100);
        };

        var getBonus = function(price) {
            var prePayment = getPrepayment(price);
            return prePayment * (33.6 / 100);
        };

        $scope.getPayForMonth = function(months, price) {
            var payForMonth = getTotally(months, price) / months;
            return (payForMonth).formatMoney(2);
        };
        $scope.getSavings = function(months, price) {
            var totallyForMonths = getTotally(months, price);
            var totallyFor12Months = getTotally(12, price);
            var saving = totallyFor12Months - totallyForMonths;

            return (saving).formatMoney(2);
        };

        Number.prototype.formatMoney = function(c, d, t) {
            var n = this;
            c = isNaN(c = Math.abs(c)) ? 2 : c;
            d = d === undefined ? '.' : d;
            t = t === undefined ? ',' : t;
            var s = n < 0 ? '-' : '',
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + '',
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
        };

    }
]);
