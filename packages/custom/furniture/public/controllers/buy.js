angular.module('mean.furniture').controller('BuyController', ['$scope', '$stateParams', '$location', 'Global', 'MeanUser', 'Furniture',
    function($scope, $stateParams, location, Global, MeanUser, Furniture) {
        $scope.global = Global;
        $scope.package = {
            name: 'furniture'
        };
        $scope.prePayment = false;
        $scope.quantity = 1;
        $scope.user = MeanUser.user

        $scope.monthlys = [{
            value: 3,
            str: '3 Abonos mensuales de: '
        }, {
            value: 6,
            str: '6 Abonos mensuales de: '
        }, {
            value: 9,
            str: '9 Abonos mensuales de: '
        }, {
            value: 12,
            str: '12 Abonos mensuales de: '
        }];

        console.log(MeanUser.user);

        $scope.findOne = function() {
            Furniture.get({
                furnitureId: $stateParams.furnitureId
            }, function(furniture) {
                $scope.furniture = furniture;

                $scope.$watch('quantity', function() {
                    $scope.totalPrice = $scope.getTotallyForMonth(12, furniture.price, $scope.quantity);
                    $scope.prePaymentTotal = $scope.getPrepayment($scope.totalPrice);
                    $scope.bonusTotal = $scope.getBonus($scope.totalPrice);

                    $scope.payMonth = $scope.getPayForMonth($scope.currentMonth, furniture.price, $scope.quantity);
                    $scope.totallyMonth = $scope.getTotallyForMonth($scope.currentMonth, furniture.price, $scope.quantity);
                    $scope.savings = $scope.getSavings($scope.currentMonth, furniture.price, $scope.quantity);

                });

                $scope.$watch('prePayment', function() {
                    $scope.totalPrice = $scope.getTotallyForMonth(12, furniture.price, $scope.quantity);
                });

            });
        };

        $scope.getShortDescription = function(description) {
            var des = description
            console.log(description);
            return des.trunc(100, true);
        };

        String.prototype.trunc = function(n, useWordBoundary) {
            var toLong = this.length > n,
                s_ = toLong ? this.substr(0, n - 1) : this;
            s_ = useWordBoundary && toLong ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
            return toLong ? s_ + '....;' : s_;
        };

        $scope.getInterests = function(months, interests, price) {
            return price * ((interests * months) / 100);
        };

        $scope.getRealPrice = function(months, price) {
            var interests = $scope.getInterests(months, 2.8, price);
            return price + interests;
        };

        $scope.getTotallyForMonth = function(months, price, quantity) {
            $scope.currentMonth = months;
            var realPrice = $scope.getRealPrice(months, price);
            var prePayment = $scope.getPrepayment(realPrice);
            var bonus = $scope.getBonus(realPrice);

            if ($scope.prePayment) {
                return ((realPrice - prePayment) - bonus) * quantity;
            } else {
                return realPrice * quantity;
            }
        };

        $scope.getPrepayment = function(price) {
            return price * (20 / 100);
        };

        $scope.getBonus = function(price) {
            var prePayment = $scope.getPrepayment(price);
            return prePayment * (33.6 / 100);
        };

        $scope.getPayForMonth = function(months, price, quantity) {
            var payForMonth = $scope.getTotallyForMonth(months, price, quantity) / months;
            return payForMonth;
        };
        $scope.getSavings = function(months, price, quantity) {
            var totallyForMonths = $scope.getTotallyForMonth(months, price, quantity);
            var totallyFor12Months = $scope.getTotallyForMonth(12, price, quantity);
            var saving = totallyFor12Months - totallyForMonths;

            return saving;
        };



    }
]);
