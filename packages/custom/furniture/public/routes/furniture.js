'use strict';

angular.module('mean.furniture').config(['$stateProvider',
    function($stateProvider) {

        $stateProvider
            .state('Create New Furniture', {
                url: '/furniture/livingRoom',
                templateUrl: 'furniture/views/livingRoom.html',
            })
            .state('Buy one', {
                url: '/furniture/:furnitureId/buy',
                templateUrl: 'furniture/views/buy.html',
                resolve: {
                    loggedin: function(MeanUser) {
                        return MeanUser.checkLoggedin();
                    }
                }
            }).state('Info furniture', {
                url: '/furniture/:furnitureId/info',
                templateUrl: 'furniture/views/info.html',
            })
    }
]);
