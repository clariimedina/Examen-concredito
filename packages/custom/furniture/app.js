'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Furniture = new Module('furniture');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Furniture.register(function(app, auth, database) {

    //We enable routing. By default the Package Object is passed to the routes
    Furniture.routes(app, auth, database);

    //We are adding a link to the main menu for all authenticated users

    Furniture.aggregateAsset('css', 'furniture.css');

    /**
      //Uncomment to use. Requires meanio@0.3.7 or above
      // Save settings with callback
      // Use this for saving data from administration pages
      Furniture.settings({
          'someSetting': 'some value'
      }, function(err, settings) {
          //you now have the settings object
      });


      // Another save settings example this time with no callback
      // This writes over the last settings.
      Furniture.settings({
          'anotherSettings': 'some value'
      });

      // Get settings. Retrieves latest saved settigns
      Furniture.settings(function(err, settings) {
          //you now have the settings object
      });
      */

    return Furniture;
});
